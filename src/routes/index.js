import React from "react";
import {
    BrowserRouter,
    Switch,
    Route,
  } from "react-router-dom";
import {SplashscreenPage,HomePage,LoginPage, SearchPage, ChatPage, UploadPage, ProfilePage, ProfileOtherPage} from "../pages";
import DetailPhoto from "../pages/Profile/detailPhoto";

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/spashcreen" exact>
          <SplashscreenPage />
        </Route>
        <Route path="/" exact>
          <LoginPage />
        </Route>
        <Route path="/home" exact>
          <HomePage />
        </Route>
        <Route path="/chat" exact>
          <ChatPage />
        </Route>
        <Route path="/search" exact>
          <SearchPage />
        </Route>
        <Route path="/upload" exact>
          <UploadPage />
        </Route>
        <Route path="/profile" exact>
          <ProfilePage />
        </Route>
        <Route path="/profile/detail" exact>
          <DetailPhoto/>
        </Route>
        <Route path="/profileother" exact>
          <ProfileOtherPage/>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Router;

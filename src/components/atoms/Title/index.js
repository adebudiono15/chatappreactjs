import React from 'react'
import Helmet from 'react-helmet'

const Title = (props) => {
  return (
    <div>
        <Helmet>
            <meta charSet="utf-8" />
            <title>{props.title} | Subluv</title>
            <link rel="canonical" href="http://mysite.com/example" />
        </Helmet>
    </div>
  )
}

export default Title
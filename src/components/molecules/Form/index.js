import React from 'react'
import {InputGroup,FormControl,Image} from 'react-bootstrap'
import styles from './styles.module.css'

const FormComponent = (props) => {
  return (
    <div style={{ textAlign:'-webkit-center' }}>
        <InputGroup style={{ width: props.width === '' ? 327 : props.width }}>
                <InputGroup.Text className={styles.textInputGroupStyle}>
                        <Image src={props.icon} width={props.widthIcon} />
                </InputGroup.Text>
                <FormControl 
                style={{ height:45 }}
                type={props.type}
                className={styles.formControlStyle}
                placeholder={props.placeholder}
                security={props.security}
                onChange={props.onChange}
                value={props.value}
                name={props.name}
                />
                {props.Button}
        </InputGroup>
    </div>
  )
}

export default FormComponent
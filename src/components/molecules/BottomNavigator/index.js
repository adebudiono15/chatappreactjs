import React from 'react'
import { Container,Row,Col,Image,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { useLocation,useHistory } from 'react-router-dom'
import { IconMenuActive, IconMenuNonActive, IconProfileActive, IconProfileNonActive, IconSearchActive, IconSearchNonActive, IconUploadActive, IconUploadNonActive } from '../../../assets/icons'

const BottomNavigator = () => {
    let location = useLocation()
    const history = useHistory()
  return (
    <div  className={styles.rowBottom}>
            <Container>
                <Row>
                        <Col
                        style={{
                            justifyContent: 'space-between',
                            display: 'flex',
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}
                        >
                        <Image className={styles.btnMenu} src={location.pathname === '/home' ? IconMenuActive : IconMenuNonActive} onClick={()=>history.push('/home')}/>
                        <Image className={styles.btnMenu} src={location.pathname === '/search' ? IconSearchActive : IconSearchNonActive} onClick={()=>history.push('/search')}/>
                        <Image className={styles.btnMenu} src={location.pathname === '/upload' ? IconUploadActive : IconUploadNonActive} onClick={()=>history.push('/upload')}/>
                        <Image className={styles.btnMenu} src={location.pathname === '/profile' ? IconProfileActive : IconProfileNonActive} onClick={()=>history.push('/profile')}/>
                        </Col>
                </Row>
            </Container>
        </div>
  )
}

export default BottomNavigator
import React from 'react'

const Status = (props) => {
  return (
    <div
    style={{
            position:'absolute',
            height:12,
            width:12,
            background:props.color,
            top:0,
            right:0,
            borderRadius:'50%',
            border:'2px solid white'
    }}
    />
  )
}

export default Status
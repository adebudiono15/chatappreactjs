import React from 'react'
import { IconSearchNonActive } from '../../../assets/icons'
import FormComponent from '../Form'

const SearchComponent = (props) => {
  return (
    <div style={{paddingBottom:16,paddingTop:16}}>
        <FormComponent
         type="text"
         placeholder="Search" 
         icon={IconSearchNonActive}
         widthIcon={35}
         onChange={props.onChange}
         value={props.value}
         name={props.name}
         />
    </div>
  )
}

export default SearchComponent
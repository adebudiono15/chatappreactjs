import React from 'react'
import { Button } from 'react-bootstrap'
import styles from './styles.module.css'

const ButtonComponent = (props) => {
  return (
    <>
        <Button className={styles.buttonComponent} onClick={props.onClick}>{props.text}</Button>
    </>
  )
}

export default ButtonComponent
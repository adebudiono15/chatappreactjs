import React from 'react'
import { Container,Row,Col,Image} from 'react-bootstrap'
import { IconImg } from '../../../assets/icons'
import FormComponent from '../Form'
import styles from './styles.module.css'

const ButtomNavigatorChat = () => {
  return (
    <div  className={styles.rowBottom}>
            <Container>
                <Row>
                        <Col xs={10}>
                          <FormComponent 
                          placeholder="Aa"
                          />
                        </Col>
                        <Col>
                          <Image className={styles.iconImg} src={IconImg}/>
                        </Col>
                </Row>
            </Container>
        </div>
  )
}

export default ButtomNavigatorChat
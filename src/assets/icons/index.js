import IconEmail from './IconEmail.png';
import IconPassword from './IconPassword.png';
import IconHideEye from './IconHideEye.png';
import IconShowEye from './IconShowEye.png';
import IconGoogle from './IconGoogle.png';
import IconTwitter from './IconTwitter.png';
import IconChevLeft from './IconChevLeft.png';
import IconChevLeftWhite from './IconChevLeftWhite.png';
import IconDotVercial from './IconDotVercial.png';
import IconMenuActive from './IconMenuActive.png';
import IconMenuNonActive from './IconMenuNonActive.png';
import IconSearchActive from './IconSearchActive.png';
import IconSearchNonActive from './IconSearchNonActive.png';
import IconSearch from './IconSearch.png';
import IconUploadActive from './IconUploadActive.png';
import IconUploadNonActive from './IconUploadNonActive.png';
import IconUpload from './IconUpload.png';
import IconProfileActive from './IconProfileActive.png';
import IconProfileNonActive from './IconProfileNonActive.png';
import IconMenu from './IconMenu.png';
import IconPen from './IconPen.png';
import IconStar from './IconStar.png';
import IconStarWhite from './IconStarWhite.png';
import IconChatProfile from './IconChatProfile.png';
import IconImg from './IconImg.png';

export { 
    IconEmail, 
    IconPassword,
    IconHideEye,
    IconShowEye,
    IconGoogle,
    IconTwitter,
    IconChevLeft,
    IconChevLeftWhite,
    IconDotVercial,
    IconMenuActive,
    IconMenuNonActive,
    IconSearchActive,
    IconSearchNonActive,
    IconUploadActive,
    IconUploadNonActive,
    IconSearch,
    IconUpload,
    IconProfileActive,
    IconProfileNonActive,
    IconMenu,
    IconPen,
    IconStar,
    IconStarWhite,
    IconChatProfile,
    IconImg
};
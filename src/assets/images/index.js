import ImgLogo from './logo.svg';
import ImgLogoText from './logoText.png'
import ImgIllustrationRegister from './IllustrationRegister.png'
import ImgChat1 from './chat(1).png'
import ImgChat2 from './chat(2).png'
import ImgChat3 from './chat(3).png'
import ImgChat4 from './chat(4).png'
import ImgChat5 from './chat(5).png'
import ImgChat6 from './chat(6).png'
import ImgFrameFirst from './frameFirst.png'
import ImgFrameSecond from './frameSecond.png'
import ImgUpload from './upload.png'
import imgProfile1 from './imgProfile(1).png'
import imgProfile2 from './imgProfile(2).png'
import imgProfile3 from './imgProfile(3).png'
import imgProfile4 from './imgProfile(4).png'
import imgProfile5 from './imgProfile(5).png'
import imgProfile6 from './imgProfile(6).png'

export {
    ImgLogo,
    ImgLogoText,
    ImgIllustrationRegister,
    ImgChat1,
    ImgChat2,
    ImgChat3,
    ImgChat4,
    ImgChat5,
    ImgChat6,
    ImgFrameFirst,
    ImgFrameSecond,
    ImgUpload,
    imgProfile1,
    imgProfile2,
    imgProfile3,
    imgProfile4,
    imgProfile5,
    imgProfile6
    }
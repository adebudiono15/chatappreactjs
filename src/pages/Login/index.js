import React, { useEffect, useState } from 'react'
import { Col, Container, Row,Image,Button,Modal } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgLogoText} from '../../assets/images'
import { IconEmail, IconPassword, IconShowEye,IconHideEye, IconGoogle, IconTwitter } from '../../assets/icons'
import { ButtonComponent, FormComponent, Gap, Title } from '../../components'
import RegisterPage from '../Register'
import { useHistory } from 'react-router-dom'


const LoginPage = (props) => {
const [gretting, setGretting] = useState('Connect easily with interesting people')

const [showPassword, setShowPassword] = useState(false)
const toogleBtn = () => {
        setShowPassword(prevstate => !prevstate)
}

// Modals
const [show, setShow] = useState(false);
const handleClose = () => setShow(false);
const handleShow = () => setShow(true);
// Last Modals
const history = useHistory();
useEffect(() => {
        setTimeout(() => {
                setGretting('Welcome back!')
        }, 1400);
}, [])

  return (
<div className={styles.pages}>
        {/* Title */}
        <Title title="Login"/>
        {/* LastTitle */}
        <Container>
                <Row className='text-center'>
                <Col>
                        <Image className={styles.imagesLogo} src={ImgLogoText}/>
                </Col>
                </Row>
                <Row className='text-center'>
                        <Col>
                        <p className={styles.textTitle}>{gretting}</p>
                        </Col>
                </Row>
                {/* Gap */}
                <div style={{ marginTop:25 }} />
                {/* Last Gap */}
                {/* Email */}
                <Row className='text-center'>
                <Col>
                <FormComponent
                type="email"
                placeholder="Email" 
                icon={IconEmail}
                widthIcon={15}
                />
                </Col>
                </Row>
                {/* Last Email */}

                {/* Gap */}
                <Gap/>
                {/* Last Gap */}

                {/* Password */}
                <Row className='text-center'>
                        <Col>
                                <FormComponent
                                type={showPassword ? 'text' : 'password'}
                                placeholder="Password" 
                                icon={IconPassword}
                                widthIcon={15}
                                security="true"
                                Button={
                                <Button style={{ background:'#f9f9f9',border:'1px solid #f9f9f9'}} onClick={toogleBtn}>
                                {
                                showPassword ? <Image src={IconHideEye} width={20} /> : <Image src={IconShowEye} width={20} />
                                }
                                        
                                </Button>
                                }
                                />
                        </Col>
                </Row>
                {/* Last Password */}

                <Row className='text-end mt-3'>
                        <Col>
                                <a href="forgotpassword" style={{ textDecoration:'none' }} className={styles.textForgotPassword}>Forgot Password?</a>
                        </Col>
                </Row>

                 {/* Gap */}
                 <Gap/>
                {/* Last Gap */}

                {/* Button Login */}
                <Row className='text-center'>
                        <Col>
                                <ButtonComponent text="Login" onClick={()=> history.push('/home')}/>
                        </Col>
                </Row>
                {/* Last Button Login */}
               
                {/* Gap */}
                <Gap/>
                {/* Last Gap */}
                        
                {/* Hr */}
                <Row>
                        <Col>
                                <div style={{width: '100%', height: 13, borderBottom: '1px solid #94A694', textAlign: 'center'}}>
                                        <span style={{fontSize: 12, backgroundColor: '#fff', padding: '0 10px',color:'#94A694'}}>
                                        or continue with
                                        </span>
                                </div>
                        </Col>
                </Row>
                {/* Last Hr */}

                {/* Gap */}
                <Gap/>
                {/* Last Gap */}

                <Row className="justify-content-center text-center">
                        <Col xs={3}>
                                <Button className={styles.btnSocialMedia} onClick={()=>console.log('Google')}>
                                        <Image className={styles.imgSocialMedia}src={IconGoogle} />
                                </Button>
                        </Col>
                        <Col xs={3}>
                                <Button className={styles.btnSocialMedia} onClick={()=>console.log('Twitter')}>
                                        <Image className={styles.imgSocialMedia}src={IconTwitter} />
                                </Button>
                        </Col>
                </Row>

                {/* Gap */}
                <Gap/>
                {/* Last Gap */}

                <Row className="text-center">
                        <Col>
                                <p style={{fontSize:14,color:'#4F614F'}}>Doesn't have an account? 
                                 <Button onClick={handleShow}  className={styles.btnRegister}>Register</Button></p> 
                        </Col>
                </Row>

                 {/* Gap */}
                 <Gap/>
                {/* Last Gap */}

                <Row className="text-center mb-3">
                        <Col>
                                <span> 
                                        <a href='terms' className={styles.linkFooter}>Terms {"&"} Conditions</a>  · 
                                        <a href='privacy' className={styles.linkFooter}>Privacy Policy</a> · 
                                        <a href='faq' className={styles.linkFooter}>FAQs</a>
                                </span> 
                        </Col>
                </Row>
                <Row>
                        <Col>
                        <Modal show={show} animation={true} onHide={handleClose}>
                                <Modal.Body scrollable>
                                        <RegisterPage
                                         closeModals={() =>handleClose()}
                                         />
                                </Modal.Body>
                        </Modal>
                        </Col>
                </Row>
                
        </Container>
</div>
  )
}

export default LoginPage
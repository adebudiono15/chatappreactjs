import React from 'react'
import { Col, Container, Row,Image } from 'react-bootstrap'
import styles from './styles.module.css'
import {ImgLogo} from '../../assets/images'

const Splashscreen = () => {
return (
    <div className={styles.pages}>
        <Container>
                <Row>
                    <Col>
                        <Image className={styles.imagesLogo} src={ImgLogo}/>
                    </Col>
                </Row>
        </Container>
    </div>
  )
}

export default Splashscreen
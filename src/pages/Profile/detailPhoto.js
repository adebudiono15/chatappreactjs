import React from 'react'
import {  imgProfile4} from '../../assets/images'
import { Container, Image, Row,Col } from 'react-bootstrap'
import { IconChevLeftWhite } from '../../assets/icons'
import styles from './styles.module.css'
import { useHistory } from 'react-router-dom'

const DetailPhoto = () => {
    const history = useHistory()
  return (
    <>
    <Container>
        <Row>
            <Col>
                <Image className={styles.imgChevWhite} src={IconChevLeftWhite} onClick={() => history.push('/profile') }/>
            </Col>
        </Row>
    </Container>
            <Image style={{
                height:812,
            }} src={imgProfile4}/>
    </>
  )
}

export default DetailPhoto
import React, { useState } from 'react'
import { Col, Container, Row,Image,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgFrameFirst,ImgFrameSecond} from '../../assets/images'
import { BottomNavigatorComponent, SearchComponent } from '../../components/molecules'
import { IconSearch } from '../../assets/icons'

const SearchPage = () => {

const [search, setSearch] = useState('')
const [cancel, setCancel] = useState('')

const handleCancel= () => {
        setSearch('')
        setCancel('none')
}

  return (
        <div>
                <div className="pages">
                        <Container>
                                <Row className={styles.rowSticky}>
                                        <Col>
                                                <SearchComponent
                                                value={search}
                                                onChange={e => setSearch(e.target.value)}
                                                name="search"
                                                />
                                        </Col>
                                        <Col xs={2} style={{ display:search.length > 1 ? 'block' : 'none' }}>
                                        <Button className={styles.btnCancel}  onClick={handleCancel}>
                                                        <span className={styles.textCancel}>Cancel</span>
                                        </Button>
                                        </Col>
                                </Row>
                                <div style={{ display:search.length === 0 ? 'block' : 'none' }}>
                                        <Row style={{marginTop:50,textAlign:'center',justifyContent:'center'}}>
                                                <Col xs={8}>
                                                        <Image width={220} height={227} src={IconSearch} />
                                                        <p className={styles.textBottomSearch}>Let's search something that interests you!</p>
                                                </Col>
                                        </Row>
                                </div>
                                <div style={{ display:search.length > 0 ? 'block' : 'none' }}>
                                        <Row style={{marginTop:18,paddingLeft:10,textAlign:'left'}}>
                                                <Col>
                                                        <p className={styles.textBottomSearchResult}>250 Results</p>
                                                </Col>
                                        </Row>
                                        <Row>
                                                <Col xs={4}>
                                                        <Row>
                                                                <Col>
                                                                <Image width={100} height={100} src={ImgFrameFirst}/>
                                                                </Col>
                                                        </Row>
                                                        <div style={{marginTop:10}}/>
                                                        <Row>
                                                                <Col>
                                                                <Image width={100} height={100} src={ImgFrameFirst}/>
                                                                </Col>
                                                        </Row>
                                                        <div style={{marginTop:10}}/>
                                                        <Row>
                                                                <Col>
                                                                <Image width={100} height={100} src={ImgFrameFirst}/>
                                                                </Col>
                                                        </Row>
                                                        <div style={{marginTop:10}}/>
                                                        <Row>
                                                                <Col>
                                                                <Image width={100} height={100} src={ImgFrameFirst}/>
                                                                </Col>
                                                        </Row>
                                                </Col>
                                                <Col>
                                                        <Image width={225} height={208} src={ImgFrameSecond} />
                                                        <div style={{marginTop:10}}/>
                                                        <Row>
                                                                <Col xs={6}>
                                                                        <Image width={103} height={100} src={ImgFrameSecond} />
                                                                </Col>
                                                                <Col xs={6}>
                                                                        <Image width={103} height={100} src={ImgFrameSecond} />
                                                                </Col>
                                                        </Row>
                                                        <div style={{marginTop:10}}/>
                                                        <Row>
                                                                <Col xs={6}>
                                                                        <Image width={103} height={100} src={ImgFrameSecond} />
                                                                </Col>
                                                                <Col xs={6}>
                                                                        <Image width={103} height={100} src={ImgFrameSecond} />
                                                                </Col>
                                                        </Row>
                                                </Col>
                                        </Row>
                                </div>
                        </Container>
                </div>
                <BottomNavigatorComponent/>
        </div>
  )
}

export default SearchPage
import React, { useState } from 'react'
import { Col, Container, Row,Image,Button,Modal } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgChat1,imgProfile1,imgProfile2,imgProfile3,imgProfile4,imgProfile5,imgProfile6} from '../../assets/images'
import { IconChatProfile, IconMenu, IconPen,IconStar, IconStarWhite} from '../../assets/icons'
import { BottomNavigatorComponent } from '../../components/molecules'
import { useHistory } from 'react-router-dom'

const ProfilePage = () => {
  const history = useHistory()
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
<div>
    <div className="pages">
            <Container>
                    <Row className={styles.rowSticky}>
                          <Col style={{textAlign:'left'}}>
                            <p className={styles.textHeader}>Profile</p>
                          </Col>
                          <Col style={{textAlign:'right'}}>
                            <Image width={24} height={24} style={{cursor:'pointer'}} src={IconMenu} onClick={()=> console.log('menu')}/>
                          </Col>
                    </Row>
                    {/*  */}
                    <Row className='mt-3'>
                          <Col>
                          {/*  */}
                            <Row>
                              <Col xs={3}>
                                <Image className={styles.imgProfile} src={ImgChat1}/>
                              </Col>
                              <Col>
                                <p className={styles.textNameProfile}>Dianne Russell</p>
                                <p className={styles.textNameProfileBottom}>Interactiveness</p>
                                  <span>
                                    <Image  width={24} height={24} src={IconStar}/>
                                    <Image  width={24} height={24} src={IconStar}/>
                                    <Image  width={24} height={24} src={IconStar}/>
                                    <Image  width={24} height={24} src={IconStar}/>
                                    <Image  width={24} height={24} src={IconStarWhite}/>
                                    <span className={styles.textNameProfileBottom}> 4.3/5 (12)</span>
                                  </span>
                              </Col>
                              <Col xs={2}>
                                <Image width={24} height={24} style={{cursor:'pointer'}} src={IconPen} onClick={()=> console.log('pen ')}/>
                              </Col>
                            </Row>
                            {/*  */}
                            <Row>
                              <Col>
                                <p className={styles.textContent}>Luctus vel accumsan dictum imperdiet nulla tristique nec sit feugiat. Pretium quam integer platea sagittis ullamcorper in gravida... <span className={styles.textSeeMore}>See More</span></p>
                              </Col>
                            </Row>
                            {/*  */}
                              <Row className='text-center'>
                              <Col xs={10}>
                              <Button className={styles.btnChatProfile}><span className={styles.textBtn} onClick={handleShow}>Subscription Option</span></Button>
                              </Col>
                              <Col>
                                <Image className={styles.IconChatProfile} src={IconChatProfile}/>
                              </Col>
                              {/* Modal */}
                                  <Modal show={show} animation={true} onHide={handleClose}>
                                    <Modal.Body scrollable>
                                      {/*  */}
                                            <Row className='text-center'>
                                              <Col xs={1}>
                                                <p className={styles.textX} onClick={handleClose}>x</p>
                                              </Col>
                                              <Col>
                                                <p className={styles.textTitleModals}>Choose your susbcription</p>
                                              </Col>
                                            </Row>
                                            {/*  */}
                                            {/* Content */}
                                            <Row  className={styles.rowSubs}>
                                              <Col>
                                              <p className={styles.textHours}>5 Hours</p>
                                              <p className={styles.textPricing}>$20.00</p>
                                              </Col>
                                              <Col style={{float:'right',textAlign:'right'}}>
                                                    <input
                                                    type="radio"
                                                  />
                                              </Col>
                                            </Row>
                                            {/*  */}
                                            <div style={{height:15}}/>
                                            {/*  */}
                                            <Row  className={styles.rowSubs}>
                                              <Col>
                                              <p className={styles.textHours}>24 Hours</p>
                                              <p className={styles.textPricing}>$100.00</p>
                                              </Col>
                                              <Col style={{float:'right',textAlign:'right'}}>
                                                    <input
                                                    type="radio"
                                                  />
                                              </Col>
                                            </Row>
                                            {/*  */}
                                            <div style={{height:15}}/>
                                            {/*  */}
                                            <Row  className={styles.rowSubs}>
                                              <Col>
                                              <p className={styles.textHours}>One Week</p>
                                              <p className={styles.textPricing}>$210.00</p>
                                              </Col>
                                              <Col style={{float:'right',textAlign:'right'}}>
                                                    <input
                                                    type="radio"
                                                  />
                                              </Col>
                                            </Row>
                                              {/*  */}
                                              <div style={{height:15}}/>
                                            {/*  */}
                                            <Row  className={styles.rowSubs}>
                                              <Col>
                                              <p className={styles.textHours}>30 Days</p>
                                              <p className={styles.textPricing}>$800.00</p>
                                              </Col>
                                              <Col style={{float:'right',textAlign:'right'}}>
                                                    <input
                                                    type="radio"
                                                  />
                                              </Col>
                                            </Row>
                                            {/* Last Content */}
                                    </Modal.Body>
                            </Modal>
                              {/* Last Modal */}
                            </Row>
                          </Col>
                    </Row>
                    {/*  */}
                   <div style={{paddingTop:20}}>
                   <Row>
                      <Col className={styles.colPointer} xs={4}>
                        <Image className={styles.imgContent} src={imgProfile1}  onClick={()=> history.push('/profile/detail')}/>
                      </Col>
                      <Col  className={styles.colPointer}  xs={4}>
                        <Image className={styles.imgContent} src={imgProfile2}  onClick={()=> history.push('/profile/detail')}/>
                      </Col>
                      <Col  className={styles.colPointer}  xs={4}>
                        <Image className={styles.imgContent} src={imgProfile3}  onClick={()=> history.push('/profile/detail')}/>
                      </Col>
                    </Row>
                    {/*  */}
                    <Row>
                        <Col className={styles.colPointer} xs={4}>
                          <Image className={styles.imgContent} src={imgProfile4}  onClick={()=> history.push('/profile/detail')}/>
                        </Col>
                        <Col  className={styles.colPointer}  xs={4}>
                          <Image className={styles.imgContent} src={imgProfile5}  onClick={()=> history.push('/profile/detail')}/>
                        </Col>
                        <Col  className={styles.colPointer}  xs={4}>
                          <Image className={styles.imgContent} src={imgProfile6}  onClick={()=> history.push('/profile/detail')}/>
                        </Col>
                    </Row>
                   </div>
            </Container>
    </div>
    <BottomNavigatorComponent/>
</div>
  )
}

export default ProfilePage
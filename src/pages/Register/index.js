import React, { useState } from 'react'
import { Container,Row,Col, Image,Button } from 'react-bootstrap'
import { IconChevLeft, IconEmail, IconGoogle, IconHideEye, IconPassword, IconShowEye, IconTwitter } from '../../assets/icons'
import { ImgIllustrationRegister } from '../../assets/images'
import { ButtonComponent, FormComponent, Gap } from '../../components'
import styles from './styles.module.css'

const RegisterPage = (props) => {
const [showPassword, setShowPassword] = useState(false)
const toogleBtn = () => {
        setShowPassword(prevstate => !prevstate)
}
  return (
    <>
    <Container>
        <Row>
            <Col>
                <Button className={styles.btnChevLeft} onClick={props.closeModals}>
                    <Image width={22} height={24} src={IconChevLeft} />
                </Button>
            </Col>
        </Row>

        {/* Gap */}
        <Gap/>
        {/* Last Gap */}

        <Row className='text-center'>
            <Col>
                <Image width={220} height={136} src={ImgIllustrationRegister} />
            </Col>
        </Row>

        {/* Gap */}
        <Gap/>
        {/* Last Gap */}

        <Row className='text-center'>
            <Col>
               <p className={styles.textTitleRegister}>Sign up and get chatting</p>
            </Col>
        </Row>

        {/* Gap */}
        <Gap/>
        {/* Last Gap */}

        {/* Email */}
        <Row className='text-center'>
                <Col>
                    <FormComponent
                    type="email"
                    placeholder="Email" 
                    icon={IconEmail}
                    widthIcon={15}
                    />
                </Col>
        </Row>
        {/* Last Email */}

         {/* Gap */}
         <Gap/>
        {/* Last Gap */}

        {/* Password */}
        <Row className='text-center'>
                <Col>
                        <FormComponent
                        type={showPassword ? 'text' : 'password'}
                        placeholder="Password" 
                        icon={IconPassword}
                        widthIcon={15}
                        security="true"
                        Button={
                        <Button style={{ background:'#f9f9f9',border:'1px solid #f9f9f9'}} onClick={toogleBtn}>
                        {
                        showPassword ? <Image src={IconHideEye} width={20} /> : <Image src={IconShowEye} width={20} />
                        }
                                
                        </Button>
                        }
                        />
                </Col>
        </Row>
        {/* Last Password */}

        {/* Gap */}
            <Gap/>
        {/* Last Gap */}

        {/* Button Login */}
        <Row className='text-center'>
                <Col>
                        <ButtonComponent text="Register"/>
                </Col>
        </Row>
        {/* Last Button Login */}
        
        {/* Gap */}
        <Gap/>
        {/* Last Gap */}
                
        {/* Hr */}
        <Row>
                <Col>
                        <div style={{width: '100%', height: 13, borderBottom: '1px solid #94A694', textAlign: 'center'}}>
                                <span style={{fontSize: 12, backgroundColor: '#fff', padding: '0 10px',color:'#94A694'}}>
                                or continue with
                                </span>
                        </div>
                </Col>
        </Row>
        {/* Last Hr */}

        {/* Gap */}
        <Gap/>
        {/* Last Gap */}

        <Row className="justify-content-center text-center">
                <Col xs={3}>
                        <Button className={styles.btnSocialMedia} onClick={()=>console.log('Google')}>
                                <Image className={styles.imgSocialMedia}src={IconGoogle} />
                        </Button>
                </Col>
                <Col xs={3}>
                        <Button className={styles.btnSocialMedia} onClick={()=>console.log('Google')}>
                                <Image className={styles.imgSocialMedia}src={IconTwitter} />
                        </Button>
                </Col>
        </Row>
    </Container>
    </>
  )
}

export default RegisterPage
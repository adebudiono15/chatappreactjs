import React from 'react'
import { Col, Container, Row,Image,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgUpload} from '../../assets/images'
import { IconUpload} from '../../assets/icons'
import { BottomNavigatorComponent } from '../../components/molecules'

const UploadComponent = () => {
  return (
<div>
    <div className="pages">
            <Container>
                    <Row className='mt-3'>
                          <Col>
                            <p className={styles.textHeader}>Upload Image</p>
                          </Col>
                    </Row>
                    <Row className='text-center'>
                          <Col>
                            <Image className={styles.imgUpload} src={ImgUpload}/>
                          </Col>
                    </Row>
                    <Row className='text-center mt-4 mb-0'>
                          <Col>
                            <p className={styles.textUploadContent}>Share your videos with anyone, or everyone</p>
                          </Col>
                    </Row>
                    <Row className='text-center'>
                          <Col>
                            <Button className={styles.btnUpload} onClick={()=>console.log('upload')}>
                                <p className={styles.textBtnUpload}>Upload <Image width={24} height={24} src={IconUpload} style={{marginBottom:5}} /></p>
                            </Button>
                          </Col>
                    </Row>
            </Container>
    </div>
    <BottomNavigatorComponent/>
</div>
  )
}

export default UploadComponent
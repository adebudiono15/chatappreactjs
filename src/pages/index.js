import SplashscreenPage from './Splashscreen';
import LoginPage from './Login';
import RegisterPage from './Register';
import HomePage from './Home';
import ChatPage from './Home/chat';
import SearchPage from './Search';
import UploadPage from './Upload';
import ProfilePage from './Profile';
import DetailPhotoContent from './Profile/detailPhoto';
import  ProfileOtherPage from './ProfileOther';

export {
    SplashscreenPage,
    LoginPage,
    RegisterPage,
    HomePage,
    ChatPage,
    SearchPage,
    UploadPage,
    ProfilePage,
    DetailPhotoContent,
    ProfileOtherPage
}

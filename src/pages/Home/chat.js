import React from 'react'
import { Col, Container, Row,Image,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgChat1, ImgChat6} from '../../assets/images'
import { BottomNavigatorChatComponent} from '../../components/molecules'
import { IconDotVercial,IconChevLeft } from '../../assets/icons'
import { useHistory } from 'react-router-dom'

const Detail = () => {
  const history = useHistory()
  return (
    <div>
    <div className="pages">
            <Container>
                    {/* Header */}
                    <Row className={styles.rowSticky}>
                            <Col style={{textAlign:'left'}} xs={9}>
                                  <Button className={styles.btnBack} onClick={()=> history.push('/home')}>
                                        <Image  width={22} height={24} src={IconChevLeft}/>
                                  </Button>
                                    <Image  width={32} height={32} style={{borderRadius:16,marginLeft:8}} src={ImgChat1}/>
                                    <span className={styles.nameHeaderChat}>Dianne Russell</span>
                            </Col>
                            <Col  style={{textAlign:'right'}}>
                                    <Button className={styles.btnDot}>
                                            <Image height={24} width={24} src={IconDotVercial}/>
                                    </Button>
                            </Col>
                    </Row>
                    {/* Last Header */}

                    {/* Content */}
                    <Row className={styles.bgContentChat}>
                          {/* Chat */}
                            <Col xs={12}>
                                   <div className={styles.chatRecive}>
                                     <p className={styles.chatText}>But don’t worry cause we are all learning here</p>
                                     <p className={styles.chatTextTime}>16.46</p>
                                   </div>
                            </Col>
                            {/*  */}
                            <Col xs={12}>
                                   <div className={styles.chatRecive}>
                                     <Image src={ImgChat6} width={280} height={150} />
                                     <p className={styles.chatText}>But don’t worry cause we are all learning here</p>
                                     <p className={styles.chatTextTime}>16.46</p>
                                   </div>
                            </Col>
                            {/*  */}
                            <Col xs={12}>
                                   <div className={styles.chatSend}>
                                     <Image src={ImgChat6} width={280} height={150} />
                                     <p className={styles.chatTextSend}>But don’t worry cause we are all learning here</p>
                                     <p className={styles.chatTextTimeSend}>16.46 · Read</p>
                                   </div>
                            </Col>
                            {/* Time Hr */}
                            <Col xs={12}>
                              <div>
                                <p className={styles.timeHr}>Sat, 17/10</p>
                              </div>
                            </Col>
                            {/* Last Time Hr */}
                            <Col xs={12}>
                                   <div className={styles.chatRecive}>
                                     <p className={styles.chatText}>But don’t worry cause we are all learning here</p>
                                     <p className={styles.chatTextTime}>16.46</p>
                                   </div>
                            </Col>
                            {/*  */}
                            <Col xs={12}>
                                   <div className={styles.chatSend}>
                                     <p className={styles.chatTextSend}>How is it going?</p>
                                     <p className={styles.chatTextTimeSend}>16.46 · Delivered</p>
                                   </div>
                            </Col>
                            {/* Last Chat */}
                    </Row>
                    {/* Last Content */}
            </Container>
    </div>
    <BottomNavigatorChatComponent/>
</div>
  )
}

export default Detail
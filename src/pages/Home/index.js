import React from 'react'
import { Col, Container, Row,Image,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { ImgLogoText} from '../../assets/images'
import { StatusComponent, BottomNavigatorComponent, SearchComponent } from '../../components/molecules'
import { IconDotVercial } from '../../assets/icons'
import { ImgChat1,ImgChat2,ImgChat3,ImgChat4,ImgChat5 } from '../../assets/images'
import { useHistory } from 'react-router-dom'

const HomePage = () => {
const history = useHistory()
  return (
<div>
        <div className="pages">
                <Container>
                        {/* Header */}
                        <Row className={styles.rowSticky}>
                                <Col style={{textAlign:'left'}}>
                                        <Image className={styles.imagesLogo} src={ImgLogoText}/>
                                </Col>
                                <Col style={{textAlign:'right'}}>
                                        <Image className={styles.btnMenu} src={IconDotVercial}/>
                                </Col>
                                {/* Search */}
                                <SearchComponent/>
                                {/* Last Search */}
                        </Row>
                        {/* Last Header */}

                        {/* Content */}
                        <Row className='mt-3'>
                                <Col>
                                        {/* Chat */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat1}/>
                                                                <StatusComponent color="#27AE60"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Dianne Russell</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col className={styles.message}><p>How is it going?</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                        {/* Hr */}
                                        <hr 
                                        style={{
                                                border: '1px solid #F3F3F3',
                                                margin:5,
                                        }}
                                        />
                                        {/* Last Hr */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat2}/>
                                                                <StatusComponent color="#27AE60"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Courtney Henry</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col className={styles.message}><p>Do you have planned this weekend?</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                          {/* Hr */}
                                          <hr 
                                                style={{
                                                        border: '1px solid #F3F3F3',
                                                        margin:5,
                                                }}
                                        />
                                        {/* Last Hr */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat3}/>
                                                                <StatusComponent color="#27AE60"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Devon Lane</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col className={styles.message}><p>Thanks. I’ll be back in two minutes.</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                         {/* Hr */}
                                         <hr 
                                        style={{
                                                border: '1px solid #F3F3F3',
                                                margin:5,
                                        }}
                                        />
                                        {/* Last Hr */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat2}/>
                                                                <StatusComponent color="#FF0000"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Courtney Henry</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col className={styles.message}><p>Do you have planned this weekend?</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                          {/* Hr */}
                                          <hr 
                                                style={{
                                                        border: '1px solid #F3F3F3',
                                                        margin:5,
                                                }}
                                        />
                                        {/* Last Hr */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat4}/>
                                                                <StatusComponent color="#FFCB3D"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Cody Fisher</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col className={styles.message}><p>Alright sure</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                          {/* Hr */}
                                          <hr 
                                                style={{
                                                        border: '1px solid #F3F3F3',
                                                        margin:5,
                                                }}
                                        />
                                        {/* Last Hr */}
                                        <Button className={styles.btnChat}  onClick={() => history.push('/chat')}>
                                                <Row>
                                                        <Col xs={2} style={{position:'relative'}}>
                                                                <Image className={styles.imgChat} src={ImgChat5}/>
                                                                <StatusComponent color="#94A694"/>
                                                        </Col>
                                                        <Col>
                                                        <Row>
                                                                <Col><p className={styles.nameProfile}>Wade Warren</p></Col>
                                                        </Row>
                                                        <Row>
                                                                <Col><p className={styles.message}>That's nice! See you later</p></Col>
                                                        </Row>
                                                        </Col>
                                                </Row>
                                        </Button>
                                        {/* Last Chat */}
                                        
                                </Col>
                        </Row>
                        {/* Last Content */}
                </Container>
        </div>
        <BottomNavigatorComponent/>
</div>
  )
}

export default HomePage